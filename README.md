# Projet de Détection de Falsification d'Images

Ce projet est une application de détection de falsification d'images en utilisant diverses techniques de traitement d'images et de vision par ordinateur en Python. L'objectif principal de ce projet est de développer une solution pour identifier des altérations potentielles dans des images qui pourraient indiquer une falsification.

## Structure du Projet

Le projet est organisé de la manière suivante :

- `__pycache__` : Contient des fichiers temporaires générés par Python lors de l'exécution du code.
- `data` : Contient les données nécessaires au projet, telles que les jeux de données d'images.
- `img` : Contient des images utilisées pour des tests ou des exemples.
- `Analyse.py` : Le script principal pour l'analyse des images.
- `DoG_ORB.py` : Implémente la détection des points d'intérêt à l'aide de l'algorithme Difference of Gaussians (DoG) et du descripteur ORB.
- `LBP_class.py` : Implémente la détection des points d'intérêt en utilisant le Local Binary Pattern (LBP).
- `SIFT.py` : Implémente la détection des points d'intérêt en utilisant le Scale-Invariant Feature Transform (SIFT).
- `SimpleDialogBlock.py` : Un module pour créer des boîtes de dialogue simples.
- `SimpleDialogBlockThresh.py` : Un module pour créer des boîtes de dialogue simples pour le seuillage.
- `SimpleDialogSigmaThresh.py` : Un module pour créer des boîtes de dialogue simples pour le seuillage avec sigma.
- `SimpleDialogThresh.py` : Un module pour créer des boîtes de dialogue simples pour le seuillage.
- `appli.py` : L'interface utilisateur de l'application pour lancer l'analyse sur des images sélectionnées.
- `data_sift.json` : Contient les données de prétraitement pour l'algorithme SIFT.
- `pretraitement.py` : Implémente les étapes de prétraitement des images.

## Comment Utiliser le Projet

1. Clonez ce référentiel sur votre système local.
2. Assurez-vous d'avoir toutes les dépendances Python requises installées.
3. Exécutez le script `appli.py` pour lancer l'interface utilisateur de l'application.
4. Sélectionner le type d'algorithme souhaitez
5. Sélectionnez une image à analyser et laissez-vuos guider.
5. Les résultats de l'analyse seront affichés dans l'interface utilisateur, stockées dans un fichier.json et visible dans l'application.

## Dépendances

Ce projet peut dépendre de diverses bibliothèques Python pour le traitement d'images, telles que OpenCV, NumPy, etc. Assurez-vous d'installer ces dépendances avant d'exécuter le projet.

## Améliorations Possibles

- Ajout de fonctionnalités d'analyse avancées.
- Amélioration de l'interface utilisateur.
- Optimisation des performances de traitement d'images.
- Gestion de grandes bases de données d'images.
- Intégration de modèles d'apprentissage automatique pour la détection de falsifications.
